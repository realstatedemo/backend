<?php

use Illuminate\Http\Response;

if (!function_exists('sendResponse')) {
    function sendResponse($httpCode, $data = [], $message = "", $headers = [])
    {
        return response()->json([
            'data' => $data,
            'message' => $message
        ], $httpCode, $headers);
    }
}

if (!function_exists('ok')) {
    function ok($data = [], array $headers = [])
    {
        return response()->json(['data' => $data],
            Response::HTTP_OK,
            $headers);
    }
}

if (!function_exists('sendApiJsonError')) {
    function sendApiJsonError($message, $http_code, $custom_code, $headers)
    {
        return response()->json([
            'message' => $message,
            'code' => $custom_code ? $custom_code : $http_code
        ], $http_code,
            $headers
        );
    }
}

if (!function_exists('badRequest')) {
    function badRequest($message = 'bad request', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_BAD_REQUEST;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('unauthorized')) {
    function unauthorized($message = 'unauthorized', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_UNAUTHORIZED;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('forbidden')) {
    function forbidden($message, $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_FORBIDDEN;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('notFound')) {
    function notFound($message = 'not found', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_NOT_FOUND;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('methodNotAllowed')) {
    function methodNotAllowed($message = 'methodNotAllowed', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_METHOD_NOT_ALLOWED;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('notAcceptable')) {
    function notAcceptable($message = 'notAcceptable', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_NOT_ACCEPTABLE;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}

if (!function_exists('internalServerError')) {
    function internalServerError($message = 'internalServerError', $code = null, array $headers = [])
    {
        $http_code = Response::HTTP_INTERNAL_SERVER_ERROR;
        return sendApiJsonError($message, $http_code, $code, $headers);
    }
}





